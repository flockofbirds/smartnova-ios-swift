//
//  ScanCode.swift
//  SmartNova
//
//  Created by Mac on 4/27/16.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import UIKit
import AVFoundation

class ScanCode: UIViewController,AVCaptureMetadataOutputObjectsDelegate {
    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    // Added to support different barcodes
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    
    do {
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    let input = try AVCaptureDeviceInput(device: captureDevice)
    
    // Initialize the captureSession object.
    captureSession = AVCaptureSession()
    // Set the input device on the capture session.
    captureSession?.addInput(input)
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    let captureMetadataOutput = AVCaptureMetadataOutput()
    captureSession?.addOutput(captureMetadataOutput)
    
    // Set delegate and use the default dispatch queue to execute the call back
    captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
    
    // Detect all the supported bar code
    captureMetadataOutput.metadataObjectTypes = supportedBarCodes
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
    videoPreviewLayer?.frame = view.layer.bounds
    view.layer.addSublayer(videoPreviewLayer!)
    
    // Start video capture
    captureSession?.startRunning()
    
    // Move the message label to the top view
  //  view.bringSubviewToFront(messageLabel)
    
    // Initialize QR Code Frame to highlight the QR code
    qrCodeFrameView = UIView()
    
    if let qrCodeFrameView = qrCodeFrameView {
    qrCodeFrameView.layer.borderColor = UIColor.greenColor().CGColor
    qrCodeFrameView.layer.borderWidth = 2
    view.addSubview(qrCodeFrameView)
    view.bringSubviewToFront(qrCodeFrameView)
    }
    
    } catch {
    // If any error occurs, simply print it out and don't continue any more.
    print(error)
    return
    }
        
        
}
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRectZero
        //  messageLabel.text = "No barcode/QR code is detected"
            
            print("No barcode/QR code is detected")
            
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        // Here we use filter method to check if the type of metadataObj is supported
        // Instead of hardcoding the AVMetadataObjectTypeQRCode, we check if the type
        // can be found in the array of supported bar codes.
        if supportedBarCodes.contains(metadataObj.type) {
            //        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
         
            var app_name = String()
            var app_pack_name = String()
            var clientid = String()
            var dept = String()
            var prod = String()
            var Rs = String()
            var image = String()
            
            if metadataObj.stringValue != nil {
                  //Using the returned value
            let ls = LocalStorage()
                
              if let data = metadataObj.stringValue.dataUsingEncoding(NSUTF8StringEncoding){
                   
                    do{
                   let Result = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
                        
                        ls.setRs_app_name(Result["AppName"]as! String)
                        ls.setRs_app_pack_name(Result["AppPackName"]as! String)
                        ls.setRs_client_id(Result["CLIENT_ID"]as! String)
                        ls.setRs_department(Result["DEPARTMENT"]as! String)
                        ls.setRs_image(Result["IMG"]as! String)
                        ls.setRs_product(Result["Product"]as! String)
                        ls.setRsUrl(Result["RS"]as! String)
                        
                        GotoStartView()
                    
                        
                     /*   if let appName = Result["AppName"]{
                            app_name = appName! as! String
                            ls.setRs_app_name(app_name)
                        }
                        
                        if let appPackName = Result["AppPackName"]{
                            app_pack_name = appPackName! as! String
                            ls.setRs_app_pack_name(app_pack_name)
                        }
                        
                        if let client_id = Result["CLIENT_ID"]{
                            clientid = client_id! as! String
                            ls.setRs_client_id(clientid)
                        }
                        
                        if let Dept = Result["DEPARTMENT"]{
                            dept = Dept! as! String
                            ls.setRs_department(dept)
                        }
                        
                        if let img = Result["IMG"]{
                            image = img! as! String
                            ls.setRs_image(image)
                        }
                        
                        if let product = Result["Product"]{
                            prod = product! as! String
                            ls.setRs_product(prod)
                        }
                        
                        if let rs = Result["RS"]{
                            Rs = rs! as! String
                          ls.setRsUrl(Rs)
                        }
                     */
                        
                    }catch{
                        
                   print("Error:error parsing json string")
                }
                
        print(app_name,app_pack_name,clientid, dept,image,prod,Rs)
                
                
                
                
            }
        }
    }
    
    
     
        
        
        
    func launchApp(decodedURL: String) {
        
        if presentedViewController != nil {
            return
        }
        
        if #available(iOS 8.0, *) {
            _ = UIAlertController(title: "Open App", message: "You're going to open \(decodedURL)", preferredStyle: .ActionSheet)
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 8.0, *) {
            _ = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
                if let url = NSURL(string: decodedURL) {
                    if UIApplication.sharedApplication().canOpenURL(url) {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
            })
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 8.0, *) {
            _ = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        } else {
            // Fallback on earlier versions
        }
        
        //alertPrompt.addAction(confirmAction)
        //alertPrompt.addAction(cancelAction)
        //presentViewController(alertPrompt, animated: true, completion: nil)
        
    }


}
    func GotoStartView(){
        let  ls = LocalStorage()
        
      self.performSegueWithIdentifier("show_start", sender: nil)// go to Member Not Found screen  
        
        print(ls.getRs_image())
    
    }
    
}