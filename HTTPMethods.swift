//
//  HTTPMethods.swift
//  SmartNova
//
//  Created by Edwin P. Magezi on 01/05/2016.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import Foundation

enum HTTPMethods {
    case GET
    case POST
    case PUT
    case DELETE
}
