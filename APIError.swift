//
//  APIError.swift
//  SmartNova
//
//  Created by Edwin P. Magezi on 30/04/2016.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import Foundation


enum APIError:ErrorType {
    case HostUnreachable
    case InvalidPermissions(String)
    case DataMissing(String)
}