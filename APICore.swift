//
//  APICore.swift
//  SmartNova
//
//  Created by Edwin P. Magezi on 28/04/2016.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import Foundation

class APICore: NSObject {
    
    let JSONSerializationErrorCode = "2000"
    let JSONSerializationErrorMessage = "Failed to serialize JSON"
    let unimplementedMethodErrorCode = "2001"
    let unimplementedMethodErrorMessage = "Unimplemented HTTP Method"

    
    func urlEncode(dict:NSDictionary) -> String {
        let result:NSMutableString = NSMutableString()
        if dict.count > 0 {
            var first:Bool = true
            
            for (key, value) in dict {
                if !first {
                    result.appendString("&")
                }
                
                first = false
                
                result.appendString(String(key))
                result.appendString("=")
                result.appendString(String(value))
                
            }
    }
        return result as String
}
    
    func getCharacterEncoding(strn:NSString) -> Int8 {
        return strn.cStringUsingEncoding(NSUTF8StringEncoding)[0]
    }
    
    
    func sendURLEncodedHTTPRequest(url_str:String, vars:NSDictionary, method:HTTPMethods) -> NSMutableDictionary {
        
        let vars_str:String = urlEncode(vars)
        var result:NSMutableDictionary = NSMutableDictionary()
        var complete:Bool = false
        
        // Set up URL based on url string
        let url:NSURL = NSURL(string:url_str)!
        
        // Call API based on speficied HTTP method
        switch method {
        case .GET:
            let dataTask = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) -> Void in
                
                if let urlContent = data {
                    
                    do {
                        
                        result = try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                        print("Result: ", result)
                    } catch {
                        result.setObject(self.JSONSerializationErrorCode, forKey: "ResultCode")
                        result.setObject(self.JSONSerializationErrorMessage, forKey: "ErrorMessage")
                        print("Source: APICore - ErrorMessage: ", self.JSONSerializationErrorMessage)
                    }
                }
                
                if error !== nil {
                    result.setObject(error!.code, forKey: "ResultCode")
                    result.setObject(error!.localizedDescription, forKey: "ErrorMessage")
                    print("Source: APICore - ErrorMessage: ", error!.localizedDescription)
                }
                
                complete = true
            }
            
            dataTask.resume()
        case .POST:
            let request:NSMutableURLRequest = NSMutableURLRequest.init(URL: url)
            request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
            request.HTTPBody = vars_str.dataUsingEncoding(NSUTF8StringEncoding)
            
            // Call request in session and handle the response
            let session:NSURLSession = NSURLSession.sharedSession()
            let dataTask:NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                
                if let data = data {
                    
                    if data.length > 0 && error == nil {
                        do {
                            result = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSMutableDictionary
                            print("Result: ", result)
                            
                        } catch {
                            result.setObject(self.JSONSerializationErrorCode, forKey: "ResultCode")
                            result.setObject(self.JSONSerializationErrorMessage, forKey: "ErrorMessage")
                            print("Source: APICore - ErrorMessage: ", self.JSONSerializationErrorMessage)
                        }
                        
                    }
                }
                
                if error !== nil {
                    result.setObject(error!.code, forKey: "ResultCode")
                    result.setObject(error!.localizedDescription, forKey: "ErrorMessage")
                    print("Source: APICore - ErrorMessage: ", error!.localizedDescription)
                }
                
                complete = true
            }
            
            // Run Request
            dataTask.resume()
        default:
            result.setObject(self.unimplementedMethodErrorCode, forKey: "ResultCode")
            result.setObject(self.unimplementedMethodErrorMessage, forKey: "ErrorMessage")
        }
        
        
        while !complete {}
        print("Final Result: ", result)
        return result
        
        
    }
    
    func GetAppModuleSettings() {
        let url_str:String = "http://154.0.170.91:8081/rest/GetAppModuleSettings"
        let vars:NSMutableDictionary = NSMutableDictionary()
        vars.setObject("com.mansystems.app.ios", forKey: "AppPackName")
        vars.setObject("2345", forKey: "EmployeeID")
        
        let result:NSDictionary = sendURLEncodedHTTPRequest(url_str, vars: vars, method: .POST)
    }
}
