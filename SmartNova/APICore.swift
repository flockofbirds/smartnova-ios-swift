//
//  APICore.swift
//  SmartNova
//
//  Created by Edwin P. Magezi on 28/04/2016.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import Foundation

class APICore: NSObject {

    
    func urlEncode(dict:NSDictionary) -> String {
        var result:NSMutableString = NSMutableString()
        if dict.count > 0 {
            var first:Bool = true
            
            for var (key, value) in dict {
                if !first {
                    result.appendString("&")
                }
                
                first = false
                
                result.appendString(String(key))
                result.appendString("=")
                result.appendString(String(value))
                
            }
    }
        return result as String
}
    
    func getCharacterEncoding(strn:NSString) -> Int8 {
        return strn.cStringUsingEncoding(NSUTF8StringEncoding)[0]
    }
    
    
    func sendURLEncodedHTTPPostRequest(url_str:String, vars:NSDictionary) -> Void {
        
        let vars_str:String = urlEncode(vars)
        
        // Set up URL and request
        let url:NSURL = NSURL(string:url_str)!
        let request:NSMutableURLRequest = NSMutableURLRequest.init(URL: url)
        request.HTTPMethod = "POST"
        //request.HTTPShouldHandleCookies = false
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type")
        request.HTTPBody = vars_str.dataUsingEncoding(NSUTF8StringEncoding)
        
        // Call request in session
        let session:NSURLSession = NSURLSession.sharedSession()
        let dataTask:NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            let data_unwrapped = data!
            if data_unwrapped.length > 0 && error == nil {
                do {
                    let result:NSDictionary = try NSJSONSerialization.JSONObjectWithData(data_unwrapped, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    print("Result: ", result)
                } catch {
                    print(error)
                }
                
            }
        }
        
        // Run Request
        dataTask.resume()
    }
    
    func GetAppModuleSettings() {
        let url_str:String = "http://154.0.170.91:8081/rest/GetAppModuleSettings"
        let vars:NSMutableDictionary = NSMutableDictionary()
        vars.setObject("com.mansystems.app.ios", forKey: "AppPackName")
        vars.setObject("2345", forKey: "EmployeeID")
        
        sendURLEncodedHTTPPostRequest(url_str, vars: vars)
    }
}
