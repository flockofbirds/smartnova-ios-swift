//
//  LoginViewController.swift
//  SmartNova
//
//  Created by Mac on 4/21/16.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let ls = LocalStorage()
    
     @IBOutlet var cellNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var core:APICore = APICore()
        core.GetAppModuleSettings()
    }
    
    @IBAction func LoginNext(sender:AnyObject) {
        // what happens when the next button is clicked on Login screen
        let url = NSURL(string: ls.getRsUrl() + "AuthenticationIOS?")!  //change url
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) -> Void in
            
            if let urlContent = data {
            
                do {
            
//                    let Result = try NSJSONSerialization.JSONObjectWithData(urlContent, options:         NSJSONReadingOptions.MutableContainers)
                 let Result = try NSJSONSerialization.JSONObjectWithData(urlContent, options: NSJSONReadingOptions.AllowFragments)
                    
                 //let ResultMessage = Result["ResultMessage"]
                    
                    if let ResultCode = Result["ResultCode"]{
                        
                        if Int(String(ResultCode!)) == 200 || Int(String(ResultCode!)) == 0 {
                            
                          self.performSegueWithIdentifier("password_view", sender: nil)// go to password screen
                           
                        
                        } else {
                        
                          self.performSegueWithIdentifier("member_not_found", sender: nil)// go to Member Not Found screen
                        
                        }
    
                        
                    }

                    
                    
                    
                    
                } catch {
                    
                    print("JSON Object Serialization Failed")
                    print(error)
            
                }
            
            
            }
            
            
            
        }
        
        task.resume()

        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
