//
//  LocalStorage.swift
//  SmartNova
//
//  Created by Edwin P. Magezi on 27/04/2016.
//  Copyright © 2016 Mansystems. All rights reserved.
//

import Foundation

   //Global Variabes
var link_url = "to be removed"

    //Urls



 // Result codes
   var ResultOk = 0
   var ResultFailed = 400



class LocalStorage: NSObject {
    // Declare class variables
    var userDefaults:NSUserDefaults
    
    override init() {
        userDefaults = NSUserDefaults.standardUserDefaults()
    }
    
    func setHasActivated(hasActivated:Bool) {
        userDefaults.setObject(hasActivated, forKey: "hasActivated")
    }
    
    func getHasActivated() -> Bool {
        return userDefaults.objectForKey("hasActivated") as! Bool
    }
    
    func setAutoLogin(autoLogin:Bool) {
        userDefaults.setObject(autoLogin, forKey: "autoLogin")
    }
    
    func getAutoLogin() -> Bool {
        return userDefaults.objectForKey("autoLogin") as! Bool
    }
    
    func setIDNumber(idNumber:String) {
        //TODO: trim string
        if idNumber.isEmpty {
            return
        }
        userDefaults.setObject(idNumber, forKey: "idNumber")
    }
    
    func getIDNumber() -> String {
        return userDefaults.objectForKey("idNumber") as! String
    }
    
    func setPassword(password:String) {
        if password.isEmpty {
            return
        }
        userDefaults.setObject(password, forKey: "password")
    }
    
    func getPassword() -> String {
        return userDefaults.objectForKey("password") as! String
    }
    
    func setMobileNumber(mobileNumber:String) {
        if mobileNumber.isEmpty {
            return
        }
        userDefaults.setObject(mobileNumber, forKey: "mobileNumber")
    }
    
    func getMobileNumber() -> String {
        return userDefaults.objectForKey("mobileNumber") as! String
    }
    
    func setDivision(division:String) {
        if division.isEmpty {
            return
        }
        userDefaults.setObject(division, forKey: "division")
    }
    
    func getDivision() -> String {
        return userDefaults.objectForKey("division") as! String
    }
    
    func setEmail(email:String) {
        if email.isEmpty {
            return
        }
        userDefaults.setObject(email, forKey: "email")
    }
    
    func getEmail() -> String {
        return userDefaults.objectForKey("email") as! String
    }
  
    func setRsUrl(rs_url:String) {
        if rs_url.isEmpty {
            return
        }
        userDefaults.setObject(rs_url, forKey: "rs_url")
    }
    
    func getRsUrl() -> String {
        return userDefaults.objectForKey("rs_url") as! String
    }
    
    
    
    func setRs_app_name(rs_app_name:String) {
        if rs_app_name.isEmpty {
            return
        }
        userDefaults.setObject(rs_app_name, forKey: "rs_app_name")
    }
    
    func getRs_app_name() -> String {
        return userDefaults.objectForKey("rs_app_name") as! String
    }
    
    
    
    func setRs_client_id(rs_client_id:String) {
        if rs_client_id.isEmpty {
            return
        }
        userDefaults.setObject(rs_client_id, forKey: "rs_client_id")
    }
    
    func getRs_client_id() -> String {
        return userDefaults.objectForKey("rs_client_id") as! String
    }
    
    
    func setRs_department(rs_department:String) {
        if rs_department.isEmpty {
            return
        }
        userDefaults.setObject(rs_department, forKey: "rs_department")
    }
    
    func getRs_department() -> String {
        return userDefaults.objectForKey("rs_department") as! String
    }
    
    
    func setRs_image(rs_image:String) {
        if rs_image.isEmpty {
            return
        }
        userDefaults.setObject(rs_image, forKey: "rs_image")
    }
    
    func getRs_image() -> String {
        return userDefaults.objectForKey("rs_image") as! String
    }
    
    
    func setRs_product(rs_product:String) {
        if rs_product.isEmpty {
            return
        }
        userDefaults.setObject(rs_product, forKey: "rs_product")
    }
    
    func getRs_product() -> String {
        return userDefaults.objectForKey("rs_product") as! String
    }
    
    
    
    func setRs_app_pack_name(rs_app_pack_name:String) {
        if rs_app_pack_name.isEmpty {
            return
        }
        userDefaults.setObject(rs_app_pack_name, forKey: "rs_app_pack_name")
    }
    
    func getRs_app_pack_name() -> String {
        return userDefaults.objectForKey("rs_app_pack_name") as! String
    }
    
    
    

}

